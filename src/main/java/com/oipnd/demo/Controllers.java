package com.oipnd.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Slf4j
public class Controllers {
    @RequestMapping("/test")
    public String   test(){
        return "test";
    }
}
