package com.oipnd.demo.examples.AtomicExample;

import com.oipnd.demo.annoations.NotThreadSafe;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *AtimicInteger通过CAS实现线程安全，同理的还有long及double等类型。
 * jdk 8中新加入了longadder，参考example2
 */
@Slf4j
@NotThreadSafe
public class Example1 {

    /*请求总数*/

    public static int clientTotal = 5000;

    /*并发数*/

    public static int threadToatal = 50;

    public static AtomicInteger count = new AtomicInteger(0);

    public static void main(String[] args) throws Exception {

        ExecutorService executorService = Executors.newCachedThreadPool();
        final Semaphore semaphore = new Semaphore(threadToatal);
        final CountDownLatch countDownLatch = new CountDownLatch(clientTotal);
        for (int i = 0; i < clientTotal; i++) {
            executorService.execute(() -> {
                try {
                    semaphore.acquire();
                    add();
                    semaphore.release();
                } catch (Exception e) {
                    log.error("exception", e);
                }
                countDownLatch.countDown();
            });
        }

        countDownLatch.await();
        executorService.shutdown();
        log.info("count:{}", count.get());

    }

    public static void add() {
        count.incrementAndGet();
    }
}
