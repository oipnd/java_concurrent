package com.oipnd.demo.examples.SafePublish;

import com.oipnd.demo.annoations.NotThreadSafe;

/**
 * 单例-饿汉模式
 * 单例实例在类装载时创建，是线程安全的，
 * 不足就是如果类的初始如果包含很多处理，就造成很大性能浪费。
 */

@NotThreadSafe
public class SingletonExample2 {

    //私有构造函数
    private SingletonExample2(){
    }

    //单例对象
    private static SingletonExample2 instance=null;

    //静态工厂方法
    private static SingletonExample2 getInstance(){
        return instance;
    }
}
