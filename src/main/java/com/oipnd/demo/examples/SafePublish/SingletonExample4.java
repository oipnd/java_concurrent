package com.oipnd.demo.examples.SafePublish;

import com.oipnd.demo.annoations.NotThreadSafe;

/**
 * 单例-懒汉模式
 * 单例实例仅在第一次使用时创建
 * 懒汉模式线程安全非synchronized改造
 * 但该模式并不是线程安全的，因为new 不是一个原子操作，此时，cpu操作指令如下：
 * 1.memory=allocate（）  分配内存空间
 * 2.ctorInstance       初始化对象
 * 3.instance=memory    设置instance指向刚分配的内存
 *
 *
 * 但是由于jvm和cpu优化，发生了指令重排。（考虑流水线操作，2,3步骤可以同步执行，并不是互斥操作。可以通过volatile来禁止指令重排。
 * 见example5
 *
 *
 * 1.memory=allocate（）  分配内存空间
 * 3.instance=memory    设置instance指向刚分配的内存
 * 2.ctorInstance       初始化对象
 */

@NotThreadSafe
public class SingletonExample4 {

    //私有构造函数
    private SingletonExample4(){
    }

    //单例对象
    private static SingletonExample4 instance=null;

    //静态工厂方法
    private static SingletonExample4 getInstance(){
        if (instance==null){    //双重检测机制
            synchronized (SingletonExample4.class){     //同步锁
                if (instance==null ){
                    instance=new SingletonExample4();
                }
            }
        }

        return instance;
    }
}
