package com.oipnd.demo.examples.SafePublish;

import com.oipnd.demo.annoations.Recommend;
import com.oipnd.demo.annoations.ThreadSafe;

/**
 * 枚举模式：最安全,最推荐
 *
 */

@ThreadSafe
@Recommend
public class SingletonExample7 {
    //私有构造函数
    private SingletonExample7(){

    }

    public static SingletonExample7 getInstance(){
        return  Singelton.INSTANCE.getInstance();
    }


    private enum Singelton{
        INSTANCE;

        private SingletonExample7 singleton;
        //JVM保证这个方法只被调用一次
        Singelton(){
            singleton=new SingletonExample7();
        }

        public SingletonExample7   getInstance(){
            return singleton;
        }
    }
}
