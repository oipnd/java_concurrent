package com.oipnd.demo.examples.SafePublish;

import com.oipnd.demo.annoations.NotRecommend;
import com.oipnd.demo.annoations.ThreadSafe;

/**
 * 单例-懒汉模式
 * 单例实例仅在第一次使用时创建
 *  但是仅可在单线程模式下正常运行，在多线程环境下是线程不安全的，主要在于工厂方法中的if判断。
 *  但可以添加synchronized，但造成比较大的性能开销。
 */

@ThreadSafe
@NotRecommend
public class SingletonExample3 {

    //私有构造函数
    private SingletonExample3(){
    }

    //单例对象
    private static SingletonExample3 instance=null;

    //静态工厂方法
    private static synchronized SingletonExample3 getInstance(){
        if (instance==null){
            instance=new SingletonExample3();
        }
        return instance;
    }
}
