# 不可变对象
## final关键字 ：修饰类、方法、变量
+ 修饰类：不能被继承，所有方法被默认为final。很少见。
- 修饰方法：1.锁定方法不能被继承类修改；2：效率（jdk1.5后这个优点没有了，以前是转为了内嵌调用）
- 修饰基本变量：1.基本数据变量（初始化后不可变）
               2.引用数据变量 （初始化后不可指向其他对象）

## 不可变对象：
- Collections.unmodifiableXXX:Collection、List、Set、Map等，此时不可使用final关键字
- Guava：ImmutableXXX：Collection、List、Set、Map等，仅可在初始化时进行赋值，此时可以使用final关键字 
