package com.oipnd.demo.examples.immutable;

import com.google.common.collect.Maps;
import com.oipnd.demo.annoations.ThreadSafe;
import lombok.extern.slf4j.Slf4j;

import java.util.Collections;
import java.util.Map;

@Slf4j
@ThreadSafe
public class ImmutableExample1 {
    private static Map<Integer,Integer> map=Maps.newHashMap();

    static {
        map.put(1,1);
        map.put(3,1);
        map.put(2,3);

        //使用Collections.unmodifiable来实现不可变更。原理：
        //生成新的一个map，然后将原map中的方法做成全部异常抛出。

        map= Collections.unmodifiableMap(map);
    }

    public static void main(String[] args) {
        map.put(4,2);

        //执行结果会抛出异常，java.lang.UnsupportedOperationException
        log.info("{}",map.get(1));
    }



}
