package com.oipnd.demo.examples.immutable;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

public class ImmutableExample2 {

    private final static ImmutableList<Integer> list=ImmutableList.of(1,2,3);

    private final static ImmutableMap<Object, Object> map=ImmutableMap.builder().put(1,2)
            .put(2,3).put(3,5).build();

    private final static ImmutableSet<Integer> set=ImmutableSet.copyOf(list);

    public static void main(String[] args) {

        //若选用ImmutabLiSt类系统，IDE即可检测出，
        //若选用List类型，会在运行时抛出异常，上者也可以抛出。
        list.add(1);
        map.put(1,2);
    }

}
