package com.oipnd.demo.examples.threadLocal;

/**
 * 获取线程id,ThreadLocal为变量在每个线程中都创建了一个副本，那么每个线程可以访问自己内部的副本变量。
 * ThreadLocal提供了线程的局部变量，
 * 每个线程都可以通过set()和get()来对这个局部变量进行操作，
 * 但不会和其他线程的局部变量进行冲突，实现了线程的数据隔离～
 */
public class RequestHolder {

    private final static ThreadLocal<Long>  requestHolder=new ThreadLocal<>();

    public static void add(long id){
        //set()用来设置当前线程中变量的副本
        //
        requestHolder.set(id);
    }

    public static Long getId(){
        //get()方法是用来获取ThreadLocal在当前线程中保存的变量副本
        return requestHolder.get();
    }

    public static void remove(){
        // remove()用来移除当前线程中变量的副本
        //必须要做，否则会发生内存泄漏。
        requestHolder.remove();
    }
}
