package com.oipnd.demo.sync;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.*;
import java.util.concurrent.Executors;

/**
 * synchronized：不可终端锁，适合竞争不激烈，可读性好
 * Lock：可中断锁，多样化同步，竞争激烈时能维持常态
 * Atomic：竞争激烈时能维持常态，比Lock性能好，但是只能同步一个值。
 *
 *
 *
 * synchronized修饰代码块只是针对对象来说。
 * 分为四种情况：
 * 1.修饰代码块：整个代码块，作用于----调用的对象
 * 2.修饰方法：整个方法，作用于----调用对象
 * 3.修饰静态方法：整个静态方法，作用于----所有对象
 * 4.修饰括号部分：整个括号部分，作用于所有对象
 */
@Slf4j
public class SyncExample1 {

    public synchronized void test1(){
        for (int i=0;i<10;i++){
            log.info("test1-{}",i);
        }
    }

    public void test2(){
        synchronized (this){
            for (int i=0;i<10;i++){
                log.info("test2-{}",i);
            }
        }
    }

    public static void main(String[] args) {
        SyncExample1    syncExample1=new SyncExample1();
        SyncExample1    syncExample2=new SyncExample1();
        ExecutorService executorService=Executors.newCachedThreadPool();
        executorService.execute(()->{
            syncExample1.test1();
        });
        executorService.execute(()->{
            syncExample2.test2();
        });

    }


}
